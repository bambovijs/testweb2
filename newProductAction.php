<?php
include 'head.php';
include 'navigation.php';
include 'productClass.php';
include 'discClass.php';
include 'bookClass.php';
include 'furnitureClass.php';

//properties for products from 'newProduct.php'
$id = $_POST["id"];
$name = $_POST["name"];
$price = $_POST["price"];
$type = $_POST["category_id"];
$size = $_POST["size"];
$weight = $_POST["weight"];
$height = $_POST["height"];
$width = $_POST["width"];
$length = $_POST["length"];

//In action adding products to database

if($type == 1){
    $disc = new Disc($id, $name, $price, $type, $size);
    //var_dump($disc);
    $disc->addProductToDB();
}
elseif($type == 2){
    $book = new Book($id, $name, $price, $type, $weight);
    //var_dump($book);
    $book->addProductToDB();

}
elseif($type == 3){
    $furniture = new Furniture($id, $name, $price, $type, $height, $width, $length);
    //var_dump($furniture);
    $furniture->addProductToDB();
}
else{
    echo "Error!";
}



?>