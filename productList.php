<?php
include 'head.php';
include 'navigation.php';
include 'productClass.php';
include 'discClass.php';
include 'bookClass.php';
include 'furnitureClass.php';

//$disc = new Disc(1, "Name", 100, 1, 700);
//$book = new Book(2, "Book", 12, 2, 1.2);
//$furniture = new Furniture(3, "Chair", 43, 3, 120, 43, 50);
//$disc->printProduct();
//$book->printProduct();
//$furniture->printProduct();

$productList = array();
$productList = readDiscsFromDB();
//$productList = readBooksFromDB();
//$productList = readFurnitureFromDB();

print '<div class="container">';
print '<div class="row">';
for($i = 0; $i < count($productList); $i++){
    $newObj = $productList[$i];
    $newObj->printProduct();
}
print '</div>';
print '</div>';




function readDiscsFromDB(){
    $discArray = array();
    // Attempt to connect to MySQL database
    $conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

    // Check connection
    if($conn === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }
    //SQL query to get all Disc products
    $sql = "SELECT p.id, p.name, p.price, p.category_id, d.size
            FROM product as p 
            JOIN disc as d ON p.id = d.product_id
    ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
            while($row = $result->fetch_assoc()) {
                $disc = new Disc($row["id"], $row["name"], $row["price"], $row["category_id"], $row["size"]);
                array_push($discArray, $disc);
            }
    }
    return $discArray;
}

function readBooksFromDB(){
    $bookArray = array();
    // Attempt to connect to MySQL database
    $conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

    // Check connection
    if($conn === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }
    //SQL query to get all Book products
    $sql = "SELECT p.id, p.name, p.price, p.category_id, b.weight
            FROM product as p 
            JOIN book as b ON p.id = b.product_id
    ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $book = new Book($row["id"], $row["name"], $row["price"], $row["category_id"], $row["weight"]);
            array_push($bookArray, $book);
        }
    }
    return $bookArray;
}

function readFurnitureFromDB(){
    $furnitureArray = array();
    // Attempt to connect to MySQL database
    $conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

    // Check connection
    if($conn === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }
    //SQL query to get all Fruniture products
    $sql = "SELECT p.id, p.name, p.price, p.category_id, f.height, f.width, f.length
            FROM product as p 
            JOIN furniture as f ON p.id = f.product_id
    ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $furniture = new Furniture($row["id"], $row["name"], $row["price"], $row["category_id"], $row["height"], $row["width"], $row["length"]);
            array_push($furnitureArray, $furniture);
        }
    }
    return $furnitureArray;
}
?>

<script>
    //Add active class to Product List page in main menu
    $(document).ready(function () {
        $("#productList").addClass('active');
    })
</script>