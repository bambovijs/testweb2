<?php

//Disc class declaration with inheritance from Product class
class Disc extends Product{
//variable for special attribute
    private $size;

    // constructor for Disc object
    public function __construct($id, $name, $price, $type, $size){
        parent::__construct($id, $name, $price, $type);
        $this->size = $size;
    }

    //getter for special attribute 'size'
    public function getSize()
    {
        return $this->size;
    }

    function printProduct()
    {
//        print '<div class="container">';
//            print '<div class="row">';
                print '<div class="col-sm-3" style="margin-top: 10px;">';
                     print '<div class="card bg-light content">';
                        print '<div class="card-body text-center">';
                            print '<div class="checkbox" style="margin: 0 0 0 10px;">';
                                print '<label><input type="checkbox" value=""></label>';
                                print '<p class="card-text">' .$this->getID(). '</p>';
                                print '<p class="card-text">' .$this->getProductName(). '</p>';
                                print '<p class="card-text">' .$this->getPrice(). '$</p>';
                                print '<p class="card-text">Size:  ' .$this->getSize(). ' MB</p>';
                            print '</div>';
                         print '</div>';
                      print '</div><br>';
                 print '</div>';
//            print '</div>';
//        print '</div>';
    }

    function addProductToDB()
    {
        //atributes for adding object to database
        $id = $this->getID();
        $name = $this->getProductName();
        $type = $this->getType();
        $price= $this->getPrice();
        $size = $this->getSize();
        $lastID = $id;

        //connection to db
        $conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

        /*  1.insert product to database,
            2. If product added, then just add info about disc
            3. output: disc added or error message*/
        $sql = "INSERT INTO product (id, name, price, category_id)
        					VALUES ('$id', '$name', '$price', '$type')";
        if ($conn->query($sql) === TRUE) {
            $sql1 = "INSERT INTO disc (id, size, category_id, product_id) 
        					VALUES ($id, '$size', '$type', '$lastID')";
            if($conn->query($sql1) === TRUE){
                print "<br><div class=\"container\">";
                print "<div class=\"alert alert-success\" role=\"alert\">New Disc Added!</div>";
                print "</div>";
            } else {
                print "<p>Error - cant create new Disc.</p>";
            }
        } else {
           print "<p>Error - cant create new Product.</p>";
           //print ". $sql. $conn->error; ";
        }
    }
}



?>