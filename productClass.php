<?php
include 'config.php';

class Product
{
    public $productList = array();
    //Variables for Product
    private $id;
    private $name;
    private $price;
    private $type;

    //constructor for Product class Object
    public function __construct($id, $name, $price, $type)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
    }

    //Getters to get information from Object variables
    public function getID()
    {
        return $this->id;
    }

    public function getProductName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    //Type: (1) - DVD Disc, (2) - Book, (3) - Furniture
    public function getType()
    {
        return $this->type;
    }
}


?>