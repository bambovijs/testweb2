<?php

//Furniture class declaration with inheritance from Product class
class Furniture extends Product{
    // variables for special attribute
    private $height;
    private $width;
    private $length;
    //constructor for furniture Object
    public function __construct($id, $name, $price, $type, $height, $width, $length)
    {
        parent::__construct($id, $name, $price, $type);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }
    //getters for special attribute values
    public function getHeight(){
        return $this->height;
    }

    public function getWidth(){
        return $this->width;
    }

    public function getLength(){
        return $this->length;
    }

    function printProduct()
    {
//        print '<div class="container">';
//            print '<div class="row">';
                print '<div class="col-sm-3" style="margin-top: 10px;">';
                    print '<div class="card bg-light content">';
                        print '<div class="card-body text-center">';
                            print '<div class="checkbox" style="margin: 0 0 0 10px;">';
                                print '<label><input type="checkbox" value=""></label>';
                                print '<p class="card-text">' .$this->getID(). '</p>';
                                print '<p class="card-text">' .$this->getProductName(). '</p>';
                                print '<p class="card-text">' .$this->getPrice(). '$</p>';
                                print '<p class="card-text">Dimensions: '.$this->getHeight().'x'.$this->getWidth().'x'.$this->getLength().'</p>';
                            print '</div>';
                        print '</div>';
                    print '</div><br>';
                print '</div>';
//            print '</div>';
//        print '</div>';
    }

    function addProductToDB()
    {
        //variables for furniture
        $id = $this->getID();
        $name = $this->getProductName();
        $type = $this->getType();
        $price= $this->getPrice();
        $height = $this->getHeight();
        $width = $this->getWidth();
        $length = $this->getLength();
        $lastID = $id;

        // connection to db
        $conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

        /*  1.insert product to database,
        2. If product added, then just add info about furniture
        3. output: furniture added or error message*/
        $sql = "INSERT INTO product (id, name, price, category_id)
        					VALUES ('$id', '$name', '$price', '$type')";
        if ($conn->query($sql) === TRUE) {
            $sql1 = "INSERT INTO furniture (id, height, width, length, category_id, product_id) 
        					VALUES ($id, '$height', '$width', '$length', '$type', '$lastID')";
            if($conn->query($sql1) === TRUE){
                print "<br><div class=\"container\">";
                print "<div class=\"alert alert-success\" role=\"alert\">New Furniture Added!</div>";
                print "</div>";
            } else {
                print "<p>Error -  cant create new Furniture.</p>";
            }
        } else {
            print "<p>Error -  cant create new Product.</p>";
            //print ". $sql. $conn->error; ";
        }
    }
}

?>