<?php

//Book class declaration with inheritance from Product class
class Book extends Product{
    //variable for special attribute
    private $weight;
    //constructor for Book object
    public function __construct($id, $name, $price, $type, $weight)
    {
        parent::__construct($id, $name, $price, $type);
        $this->weight = $weight;
    }
    //getter for special attribute 'weight'
    public function getWeight(){
        return $this->weight;
    }

    function printProduct()
    {
//        print '<div class="container">';
//            print '<div class="row">';
                print '<div class="col-sm-3" style="margin-top: 10px;">';
                    print '<div class="card bg-light content">';
                        print '<div class="card-body text-center">';
                            print '<div class="checkbox" style="margin: 0 0 0 10px;">';
                                print '<label><input type="checkbox" value=""></label>';
                                print '<p class="card-text">' .$this->getID(). '</p>';
                                print '<p class="card-text">' .$this->getProductName(). '</p>';
                                print '<p class="card-text">' .$this->getPrice(). '$</p>';
                                print '<p class="card-text">Weight:  ' .$this->getWeight(). '</p>';
                            print '</div>';
                        print '</div>';
                    print '</div><br>';
                 print '</div>';
//            print '</div>';
//        print '</div>';
    }

    function addProductToDB()
    {
        //variables for book obj
        $id = $this->getID();
        $name = $this->getProductName();
        $type = $this->getType();
        $price= $this->getPrice();
        $weight = $this->getWeight();
        $lastID = $id;

        //connection to db
        $conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

        /*  1.insert product to database,
        2. If product added, then just add info about book
        3. output: book added or error message*/
        $sql = "INSERT INTO product (id, name, price, category_id)
        					VALUES ('$id', '$name', '$price', '$type')";
        if ($conn->query($sql) === TRUE) {
            $sql1 = "INSERT INTO book (id, weight, category_id, product_id) 
        					VALUES ($id, '$weight', '$type', '$lastID')";
            if($conn->query($sql1) === TRUE){
                print "<br><div class=\"container\">";
                print "<div class=\"alert alert-success\" role=\"alert\">New Book Added!</div>";
                print "</div>";
            } else {
                print "<p>Error - cant create new Book.</p>";
            }
        } else {
            print "<p>Error - cant create new Product.</p>";
            //print ". $sql. $conn->error; ";
        }
    }
}

?>