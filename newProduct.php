<?php
include 'config.php';
include 'head.php';
include 'navigation.php';

echo "<div class=\"container\">";
echo "<h1>Add new Product</h1>";
echo "<p>Please fill this form!</p>";
echo "</div>";
echo "<div class=\"container\" style=\"margin-top: 20px\">";

echo "<form action=\"newProductAction.php\" method=\"post\">";
echo "<div class=\"form-group\">";
echo "<div class=\"col-4\">";

// SQL query for next product ID. Because in mysql IDs made with Auto Increment
$sql_vaicajums="SELECT * FROM product;";
$result=mysqli_query($conn,$sql_vaicajums);
//Get size of '$result' to know next Product ID
$nextPID = mysqli_num_rows($result) + 1;

echo "<label for=\"id\">Product ID</label>";
//ar disable nestrada, jo nepanjem ID
echo "<input type=\"text\" class=\"form-control\" name=\"id\" value=\"$nextPID\">";
echo "<br>";

echo "<label for=\"name\">Product Name</label>";
echo "<input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Product Name\">";
echo "<br>";

echo "<label for=\"price\">Product Price</label>";
echo "<input type=\"text\" class=\"form-control\" name=\"price\" placeholder=\"Product Price\">";
echo "<br>";

echo "<label for=\"categoty_id\">Product Type</label>";
echo "<select id=\"category_id\" name=\"category_id\" class=\"custom-select mr-ms-2\">";
echo "<option value=\"0\" selected>Choose type</option>";
echo "<option value=\"1\">DVD-disc</option>";
echo "<option value=\"2\">Book</option>";
echo "<option value=\"3\">Furniture</option>";
echo "</select><br><br>";

//Product type Fields (all are hiden)
echo "<div id=\"typeFieldDisc\">";
echo "<label for=\"size\">Disc Size</label>";
echo "<input type=\"text\" class=\"form-control\" name=\"size\" id=\"size\" placeholder=\"Disc Size\">";
echo "<small id=\"discSize\" class=\"form-text text-muted\">Disc size in MB</small>";
echo "</div>";

echo "<div id=\"typeFieldBook\">";
echo "<label for=\"weight\">Book Weight</label>";
echo "<input type=\"text\" class=\"form-control\" name=\"weight\" id=\"weight\" placeholder=\"Book Weight\">";
echo "<small id=\"weight\" class=\"form-text text-muted\">Book weight in KG</small>";
echo "</div>";

echo "<div id=\"typeFieldFurniture\">";
echo "<label for=\"dimensions\">Furniture Dimentions</label>";
echo "<input type=\"text\" class=\"form-control\" name=\"height\" id=\"height\" placeholder=\"Height\"><br>";
echo "<input type=\"text\" class=\"form-control\" name=\"width\" id=\"width\" placeholder=\"Width\"><br>";
echo "<input type=\"text\" class=\"form-control\" name=\"length\" id=\"length\" placeholder=\"Length\">";
echo "<small id=\"dimensions\" class=\"form-text text-muted\">Furniture dimensions (Height x Width x Length)</small>";
echo "</div><br>";

echo "<button type=\"submit\" class=\"btn btn-primary mb-2\">Submit</button>";

echo "</div>";

echo "</div></form>";

echo "</div>";


?>

    <script>
        $(document).ready(function () {
            //"Add New Product" active link in main menu
            $("#newProduct").addClass('active')
        })

        //Hide all typeField
        $('#typeFieldDisc').hide();
        $('#typeFieldBook').hide();
        $('#typeFieldFurniture').hide();
        //if value = '1' selected then it is DVD-disc, (1 = Disc, 2 = book, 3 = fruniture)
        $('#category_id').change(function(){
            if($(this).val() == "1"){
                $("#typeFieldBook").hide();
                $("#typeFieldFurniture").hide();
                $("#typeFieldDisc").show();
            }
            else if($(this).val() == "2" ){
                $("#typeFieldDisc").hide();
                $("#typeFieldFurniture").hide();
                $("#typeFieldBook").show();
            }
            else if($(this).val() == "3"){
                $("#typeFieldDisc").hide();
                $("#typeFieldBook").hide();
                $("#typeFieldFurniture").show();
            }
            else{
                $('#typeFieldDisc').hide();
                $('#typeFieldBook').hide();
                $('#typeFieldFurniture').hide();
            }
        });
    </script>

<?php include('bottom.php');

